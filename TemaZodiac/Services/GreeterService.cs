using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;


namespace TemaZodiac
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            var Birthday = DateTime.Parse(request.Date);
             var file=File.ReadAllLines(@"C:\Users\Diana\source\repos\TemaZodiac\TemaZodiac\Services\perioade.txt");
            //var file = File.ReadAllLines("perioade.txt");


            DateTime requestParsed;
            DateTime startParsed;
            DateTime endParsed;

            string startPeriod;
            string endPeriod;
            string sign="";

            string pattern = "MM/dd/yyyy";

            for (var i = 0; i < file.Length; i += 3)
            {
                startPeriod = file.ElementAt(i + 1).ToString();
                endPeriod = file.ElementAt(i + 2).ToString();
                sign = file.ElementAt(i).ToString();

                if (DateTime.TryParseExact(startPeriod, pattern, null, DateTimeStyles.None, out startParsed)
                   && DateTime.TryParseExact(endPeriod, pattern, null, DateTimeStyles.None, out endParsed)
                   && DateTime.TryParseExact(request.Date, pattern, null, DateTimeStyles.None, out requestParsed))
                {
                    if (
                        ((startParsed.Month == requestParsed.Month) &&( requestParsed.Day>=startParsed.Day))
                        || (endParsed.Month==requestParsed.Month) && (requestParsed.Day<=startParsed.Day))
                          
                    
                        return Task.FromResult(new HelloReply { Semn = sign });
                    

                }
          

            }
            return Task.FromResult(new HelloReply { Semn = sign });

        }
    }
}
