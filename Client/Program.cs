﻿using System;
using Grpc.Core;
using Grpc.Net.Client;
using System.Globalization;
using System.Threading.Channels;
using TemaZodiac;


namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);

            var request = new HelloRequest();

            static bool isValid(string Date)
            {
                DateTime dateTime;

                if (DateTime.TryParseExact(Date, "MM/dd/yyyy", null, DateTimeStyles.None, out dateTime) == false)
                {
                    Console.WriteLine(" INVALID DATE  \n");
                    return false;
                }
                return true;
            }
            do
            {
                Console.WriteLine("Enter your Birthday: ");
                request.Name= Console.ReadLine();

            } while (isValid(request.Name) == false);
            //nu ma lasa sa pun date deoarece am modificat name din proto implicit
            var person = new Greeter.GreeterClient(channel);
            var respone = client.SayHello(request);
            Console.WriteLine("Your Zodiac Sign is {0} ", respone.Message);
            Console.ReadKey();


        }
    }
}
    
